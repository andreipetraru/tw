package service

import domain.UploadedFile
import groovy.sql.Sql

class UploadService {
	Sql sqlService

	UploadService() {
		sqlService = SqlService.getSqlInstance()
	}

	def save(UploadedFile file) {
		def query = """INSERT INTO UPLOADED_FILE (content, contentType, name, tag, description, size, fileversion, user_id)
						VALUES (${file.content}, ${file.contenttype}, ${file.name}, ${file.tag}, ${file.description}, ${file.size},
						${file.fileversion}, ${file.user_id})
					"""
		sqlService.execute(query)
	}

	def delete(Integer selectedFileID, Integer userId) {
		def query = 'DELETE FROM UPLOADED_FILE where id = :id AND user_id = :userId'
		sqlService.execute(query, [id: selectedFileID, userId : userId])
	}

	def findAllUserFiles(Integer userId) {
		def query = 'SELECT id, contenttype, name, tag, description, size, fileversion, user_id FROM UPLOADED_FILE WHERE user_id = :userId'
		def files = []
		sqlService.eachRow(query, [userId: userId]) {
			files << new UploadedFile(it.toRowResult())
		}
		return files
	}
	
	def download(Integer selectedFileID, Integer userId) {
		def query = 'SELECT * FROM UPLOADED_FILE WHERE id = :id AND user_id = :userId'
		UploadedFile content
		sqlService.eachRow(query,  [id: selectedFileID, userId : userId]) {
			content = it.toRowResult()
		}
		return content
	}

	def getFileVersion(String name) {
		def query = "select max(fileversion) from uploaded_file where name = :name"
		def max = sqlService.firstRow(query, [name: name])
		return max[0]
	}
	
	def search(String name, Integer userId) {
		def query = 'SELECT id, contenttype, name, tag, description, size, fileversion, user_id FROM UPLOADED_FILE WHERE user_id = :userId AND name LIKE :name'
		def files = []
		sqlService.eachRow(query, [userId: userId, name: "%${name}%".toString()]) {
			files << new UploadedFile(it.toRowResult())
		}
		return files
	}
}
