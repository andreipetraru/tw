package service

import domain.User
import groovy.sql.Sql

class UserService {

	Sql sqlService

	UserService() {
		sqlService = SqlService.getSqlInstance()
	}

	def findByUsernameAndPassword(String username, String password) {
		def query = 'SELECT * FROM USER WHERE username=:username AND password=:password'
		def user
		sqlService.eachRow(query, [username: username, password: password]) {
			user = it.toRowResult()
		}
		return user
	}

	def findByUsername(String username) {
		def query = 'SELECT * FROM USER WHERE username=:username'
		def user
		sqlService.eachRow(query, [username: username]) {
			user = it.toRowResult()
		}
		return user
	}

	def save(User user) {
		def query = "insert into USER (username, password) values (${user.username}, ${user.password})"
		sqlService.execute(query)
	}
}
