package service


import groovy.sql.Sql

class SqlService {
	def static getSqlInstance() {
		def db = [url: 'jdbc:mysql://localhost:3306/upload', user: 'root', password: 'root', driver: 'com.mysql.jdbc.Driver']
		def sql = Sql.newInstance(db.url, db.user, db.password, db.driver)
		return sql
	}
}
