package util

import java.security.MessageDigest

class PasswordHash {
	static String getHash(String password) {
		return MessageDigest.getInstance('SHA-256')
				.digest(password.getBytes('UTF-8')).encodeBase64().toString()
	}
}
