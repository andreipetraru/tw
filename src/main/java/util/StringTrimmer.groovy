package util

import javax.faces.component.UIComponent
import javax.faces.context.FacesContext
import javax.faces.convert.Converter
import javax.faces.convert.FacesConverter

@FacesConverter(forClass = String.class)
class StringTrimmer implements Converter {
	@Override
	Object getAsObject(FacesContext context, UIComponent component, String value) {
		return value?.trim()
	}

	@Override
	String getAsString(FacesContext context, UIComponent component, Object value) {
		return (String) value
	}
}
