package bean

import domain.UploadedFile
import service.UploadService

import javax.annotation.PostConstruct
import javax.faces.context.ExternalContext
import javax.faces.context.FacesContext
import javax.faces.view.ViewScoped
import javax.inject.Inject
import javax.inject.Named

@Named
@ViewScoped
class FilesBean {
	@Inject
	UserSessionBean userSessionBean

	UploadService uploadService
	Integer userId
	List files
	Integer selectedFileId
	String searchBy

	def errorMessage

	@PostConstruct
	void init() {
		def user = userSessionBean.user
		if (user == null) {
			errorMessage = 'Not logged in'
			return
		}
		uploadService = new UploadService()
		userId = user.id
		files = uploadService.findAllUserFiles(userId)
	}

	def delete() {
		uploadService.delete(selectedFileId, userId)
		return NavigationBean.FILES
	}

	def download() {
		UploadedFile downloadedFile = uploadService.download(selectedFileId, userId)
		FacesContext facesContext = FacesContext.getCurrentInstance()
		ExternalContext externalContext = facesContext.getExternalContext()
		externalContext.setResponseHeader("Content-Type", downloadedFile.contenttype)
		externalContext.setResponseHeader("Content-Length", downloadedFile.size.toString())
		externalContext.setResponseHeader("Content-Disposition", "attachment;filename=${downloadedFile.name}")
		externalContext.getResponseOutputStream().write(downloadedFile.content)
		facesContext.responseComplete()
	}

	def search() {
		if (searchBy) {
			files = uploadService.search(searchBy, userId)
		}
		else {
			files = uploadService.findAllUserFiles(userId)
		}
	}

}
