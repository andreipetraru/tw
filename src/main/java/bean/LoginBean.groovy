package bean

import domain.User
import service.UserService
import util.PasswordHash

import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.inject.Inject
import javax.inject.Named
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Named
@ViewScoped
class LoginBean {
	@Inject
	UserSessionBean userSessionBean

	UserService userService
	User user

	def successMessage
	def errorMessage

	@NotNull(message = 'Username cannot be empty')
	@Size(min = 3, max = 10, message = 'Username must be between 3 and 10 characters long')
	String username
	@NotNull(message = 'Password cannot be empty')
	@Size(min = 5, max = 10, message = 'Password must be between 5 and 10 characters long')
	String password

	@PostConstruct
	void init() {
		userService = new UserService()
	}

	def login() {
		def hashedPassword = PasswordHash.getHash(password)
		def user = userService.findByUsernameAndPassword(username, hashedPassword)

		if (user == null) {
			errorMessage = 'Incorrect username/password'
			return null
		} else {
			userSessionBean.user = user
			return NavigationBean.UPLOAD
		}
	}

	String register() {
		def user = userService.findByUsername(username)

		if (user != null) {
			errorMessage = 'Username already taken'
		} else {
			def hashedPassword = PasswordHash.getHash(password)
			def userToSave = new User(username: username, password: hashedPassword)
			userService.save(userToSave)
			successMessage = 'Registration successful'
			username = null
			password = null
		}
	}
}
