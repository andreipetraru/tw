package bean

import domain.UploadedFile
import domain.User
import service.UploadService

import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.inject.Inject
import javax.inject.Named
import javax.servlet.http.Part
import javax.validation.constraints.Size

@Named
@ViewScoped
class UploadBean {
	@Inject
	UserSessionBean userSessionBean

	Part part
	UploadedFile file
	UploadService uploadService
	User user
	@Size(max = 100, message = 'Description length cannot exceed 100 characters')
	def description
	@Size(max = 20, message = 'Tag length cannot exceed 100 characters')
	def tag

	def successMessage
	def errorMessage

	@PostConstruct
	void init() {
		user = userSessionBean.user
		if (user == null) {
			errorMessage = 'Not logged in'
			return
		}
		uploadService = new UploadService()
	}

	def upload() {
		clearMessages()

		if (part.size == 0) {
			errorMessage = 'File not selected'
			return
		}

		if (part.size > 1606765) {
			errorMessage = 'File cannot exceed 1.5 mb'
			return
		}

		if (part.submittedFileName.length() > 40) {
			errorMessage = 'File name cannot exceed 40 chars'
			return
		}

		def baos = new ByteArrayOutputStream()
		def stream = part.inputStream
		byte[] buffer = new byte[part.size]
		int read = 0
		while ((read = stream.read(buffer)) != -1) {
			baos.write(buffer, 0, read)
		}
		stream.close()
		file = new UploadedFile(name: part.submittedFileName, content: baos.toByteArray(), size: part.size,
				fileversion: getFileVersion(), user_id: userSessionBean.user.id, contenttype: part.contentType,
				tag: tag, description: description)
		uploadService.save(file)
		successMessage = 'File uploaded'
		description = null
		tag = null
	}

	def getFileVersion() {
		def fileVersion = uploadService.getFileVersion(part.submittedFileName)
		if (fileVersion == null) {
			fileVersion = 0
		} else {
			fileVersion += 1
		}
		return fileVersion
	}

	def clearMessages() {
		errorMessage = null
		successMessage = null
	}
}
