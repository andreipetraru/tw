package bean

import javax.enterprise.context.RequestScoped
import javax.inject.Named

@Named
@RequestScoped
class NavigationBean {
	static String LOGIN = '/pages/login.jsf?faces-redirect=true'
	static String REGISTER = '/pages/register.jsf?faces-redirect=true'
	static String FILES ='/pages/files.jsf?faces-redirect=true'
	static String UPLOAD =  '/pages/upload.jsf?faces-redirect=true'
	static String HOME = '/pages/home.jsf?faces-redirect=true'

	def login() {
		return LOGIN
	}

	def register() {
		return REGISTER
	}

	def files() {
		return FILES
	}

	def upload() {
		return UPLOAD
	}

	def home() {
		return HOME
	}
}
