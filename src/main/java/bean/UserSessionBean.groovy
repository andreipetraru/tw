package bean

import domain.User

import javax.enterprise.context.SessionScoped
import javax.inject.Named

@Named
@SessionScoped
class UserSessionBean implements Serializable {
	User user

	def loggedIn() {
		return user != null
	}

	def logout() {
		user = null
		return NavigationBean.HOME
	}
}
