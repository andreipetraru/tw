package domain

class UploadedFile {
	Integer id
	byte[] content
	String name
	String tag
	String description
	String contenttype
	Integer size
	Integer fileversion
	Integer user_id
}
